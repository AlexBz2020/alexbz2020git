/**
 * Задание 1
 * 
 * Написать скрипт сложения, вычитания, умножения и деления двух чисел.
 * Результат каждой операции должен быть записан в переменную и выведен в консоль.
 * 
 * Пример: sum1 = 1
 *         sum2 = 2
 *         результуть сложения: 3
 *         результуть вычитания: -1
 *         результуть умножения: 2
 *         результуть деления: 0.5
 */
const sum1 = prompt('Введите первое число:'); // ЗАПРЕЩЕНО МЕНЯТЬ
const sum2 = prompt('Введите второе число:'); // ЗАПРЕЩЕНО МЕНЯТЬ

// РЕШЕНИЕ

const sumOut = +sum1 + +sum2;
const subtrOut = sum1 - sum2;
const multOut = sum1 * sum2;
const devOut = sum1 / sum2;

console.log(sumOut);
console.log(subtrOut);
console.log(multOut);
console.log(devOut);
//alert((Number(sum1)) + (Number(sum2));
